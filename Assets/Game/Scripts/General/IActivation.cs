using System;
using UnityEngine;

namespace Company.GameShooter
{
    public interface IActivation
    {
        public bool IsCanActivate { get;  }
        public Action OnBeforeActivate { get; set; }
        public Action OnFinishActivate { get; set; }
        public Action OnDeactivate { get; set; }

        public void BeforeActivate();
        public void Activate();
        public void FinishActivate();
        public void Deactivate();
    }
}