using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Company.GameShooter
{
    public interface IDamageReceiver
    {
        public void Take(int damage);
    }
}
