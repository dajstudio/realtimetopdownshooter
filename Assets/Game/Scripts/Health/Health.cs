using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Company.GameShooter
{
    [CreateAssetMenu(fileName = "HealthData", menuName = "Game/Health/HealthData")]
    public class Health : ScriptableObject
    {
        [ShowInInspector]
        public int defaultValue { get; private set; } = 5;
        [ShowInInspector]
        public int minValue { get; private set; } = 0;
        [ShowInInspector]
        public int maxValue { get; private set; } = 5;

        public int Change(ref int currentValue,int difference)
        {
            currentValue = Mathf.Clamp(currentValue + difference,minValue,maxValue);
          
            return currentValue;
        }
  

    }
}