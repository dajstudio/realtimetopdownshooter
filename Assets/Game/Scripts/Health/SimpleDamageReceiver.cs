using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Company.GameShooter
{
    [RequireComponent(typeof(HealthHolder))]
    public class SimpleDamageReceiver : MonoBehaviour, IDamageReceiver
    {
        [SerializeField]
        private HealthHolder _healthHolder;
        private void OnEnable()
        {
            if (_healthHolder == null)
            {
                Debug.LogWarning($"On {name} in component {nameof(SimpleDamageReceiver)} not setted {nameof(HealthHolder)} for cache");
                _healthHolder = GetComponent<HealthHolder>();
            }
        }
        public void Take(int damage)
        {
            if (damage <= 0)
            {
                //TODO: rotate to direction of fire and set to patrol. http://jira.company.region/taksRTDShooterN39
                return;
            }

            _healthHolder.Change(-damage);
        }
    }
}
