using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Company.GameShooter
{
    public class HealthHolder : MonoBehaviour
    {
        public Action<int> OnChangeHealthDiff;
        public Action<int> OnChangedHealth;

        public Health health;
        [SerializeField]
        private int currentValue;

        private void OnEnable()
        {
            if (health == null)
            {
                Debug.LogError($"{name}: Health is empty");
                return;
            }
            Change(health.defaultValue);
        }
        public void Change(int value)
        {
            OnChangeHealthDiff?.Invoke(value);
            health.Change(ref currentValue, value);
            OnChangedHealth?.Invoke(currentValue);
        }
    }
}