using Cysharp.Threading.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Company.GameShooter
{
    [RequireComponent(typeof(HealthHolder))]
    public class DeadBehaviour : MonoBehaviour
    {
        public Action OnDead;

        public bool isImmediatelyDestroy;
        [SerializeField]
        private HealthHolder _healthHolder;
        private void OnEnable()
        {
            CheckHealthInitizializationAsync().Forget();

        }
        public async UniTaskVoid CheckHealthInitizializationAsync()
        {
            if (_healthHolder == null)
            {
                Debug.LogWarning($"On {name} in component {nameof(DeadBehaviour)} not setted {nameof(HealthHolder)} for cache");
                _healthHolder = GetComponent<HealthHolder>();
            }

            await UniTask.WaitWhile(() => _healthHolder.health == null);

            _healthHolder.OnChangedHealth += CheckDeath;
        }
        public void CheckDeath(int health)
        {
            if (health > 0)
            {
                return;
            }

            OnDead?.Invoke();

            if (isImmediatelyDestroy)
            {
                //TODO: make throught Pool, like LeanPool
                Destroy(gameObject);
            }
        }
    }
}