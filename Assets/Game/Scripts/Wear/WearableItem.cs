using System;
using UnityEngine;

namespace Company.GameShooter
{
    public abstract class WearableItem<T> : MonoBehaviour, IActivation where T: WearableItemType
    {

        public bool IsCanActivate { get; internal set; } = true;
        public Action OnBeforeActivate { get; set; }
        public Action OnFinishActivate { get; set; }
        public Action OnDeactivate { get; set; }

        public T wearableItemType;
        public Transform _currentTransform { get; private set; }
        public virtual void BeforeActivate()
        {
            
            if (wearableItemType == null)
            {
                Debug.LogError($"{name}: {nameof(wearableItemType)} is empty");
                IsCanActivate = false;
                return;
            }

            _currentTransform = transform;
            wearableItemType.OnFinishActivate += OnFinishActivate;
            wearableItemType.OnDeactivate += OnDeactivate;
        }
        public void Activate()
        {
            BeforeActivate();
            OnBeforeActivate?.Invoke();

            if (!IsCanActivate)
            {
                return;
            }


            wearableItemType.SetCurrentTransform(_currentTransform);
            wearableItemType.Activate();

            FinishActivate();
            OnFinishActivate?.Invoke();
        }
        public virtual void FinishActivate()
        {

        }
        public virtual void Deactivate()
        {
            wearableItemType.OnFinishActivate -= OnFinishActivate;
            wearableItemType.OnDeactivate -= OnDeactivate;
            wearableItemType?.Deactivate();
        }
    }
}