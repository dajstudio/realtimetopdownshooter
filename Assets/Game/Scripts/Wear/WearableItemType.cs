using Sirenix.OdinInspector;
using System;
using UnityEngine;

namespace Company.GameShooter
{
    //TODO: add generic T:WearableItemData
    public abstract class WearableItemType : SerializedScriptableObject, IActivation
    {
        public bool IsCanActivate { get; internal set; } = true;
        public Action OnBeforeActivate { get; set; }
        public Action OnFinishActivate { get; set; }
        public Action OnDeactivate { get; set; }

        public Transform _currentTransform { get; private set; }
        public abstract void BeforeActivate();
        public void Activate()
        {
            BeforeActivate();
            OnBeforeActivate?.Invoke();

            if (!IsCanActivate)
            {
                Debug.LogError($"{name}: Can't Active");
                return;
            }

            //Activate

            FinishActivate();
            OnFinishActivate?.Invoke();
        }

        public abstract void FinishActivate();

        public abstract void Deactivate();
        public virtual void SetCurrentTransform(Transform currentTransform)
        {
            _currentTransform = currentTransform;
        }
    }
}