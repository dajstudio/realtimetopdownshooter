using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.OnScreen;
using UnityEngine.Serialization;

namespace Company.GameShooter
{
    public class OnScreenStick : OnScreenControl, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        [Title("RectTransform is zone for joystick")]
        public CanvasGroup joystickCanvasGroup;
        public RectTransform InsideCircle;
        [FormerlySerializedAs("movementRange")]
        [SerializeField]
        private float m_MovementRange = 50;

        [InputControl(layout = "Vector2")]
        [SerializeField]
        private string m_ControlPath;

        private Vector3 m_StartPos = Vector2.zero;
        private Vector2 m_PointerDownPos;

        private RectTransform _joystickRectTransform;

        public float movementRange
        {
            get => m_MovementRange;
            set => m_MovementRange = value;
        }

        protected override string controlPathInternal
        {
            get => m_ControlPath;
            set => m_ControlPath = value;
        }
        public void OnPointerDown(PointerEventData eventData)
        {
            _joystickRectTransform.position = eventData.position;
            joystickCanvasGroup.alpha = 1;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_joystickRectTransform, eventData.position, eventData.pressEventCamera, out m_PointerDownPos);
        }

        public void OnDrag(PointerEventData eventData)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_joystickRectTransform, eventData.position, eventData.pressEventCamera, out var position);
            Vector2 delta = position - m_PointerDownPos;

            delta = Vector2.ClampMagnitude(delta, movementRange);
            InsideCircle.anchoredPosition = m_StartPos + (Vector3)delta;

            Vector2 newPos = new Vector2(delta.x / movementRange, delta.y / movementRange);
            SendValueToControl(newPos);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            joystickCanvasGroup.alpha = 0;
            InsideCircle.anchoredPosition = m_StartPos;
            SendValueToControl(Vector2.zero);
        }

        private void Start()
        {
            m_StartPos = ((RectTransform)transform).anchoredPosition;
            _joystickRectTransform = joystickCanvasGroup.GetComponent<RectTransform>();
            joystickCanvasGroup.alpha = 0;
        }


    }
}