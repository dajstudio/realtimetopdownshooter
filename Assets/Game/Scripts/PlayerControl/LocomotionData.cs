using UnityEngine;

namespace Company.GameShooter
{
    [CreateAssetMenu(fileName = "LocomotiveData", menuName = "Game/Locomotive/Data")]
    public class LocomotionData : ScriptableObject
    {
        public float moveSpeed = 2.0f;
        public float rotationSpeed = 12.0f;
    }
}
