using Sirenix.OdinInspector;
using System;
using UnityEngine;

namespace Company.GameShooter
{
    [RequireComponent(typeof(CharacterController))]
    public class Locomotion : MonoBehaviour
    {
        public event Action OnStartMove;
        public event Action OnStopMove;

        [SerializeField]
        private LocomotionData _data;

        [SerializeField]
        private CharacterController _controller;

        private GameInputControl _inputActions;

        private Camera _mainCamera;
        private void Awake()
        {

            _inputActions = new GameInputControl();
            if (_controller == null)
            {
                Debug.LogWarning($"On {name} in component {nameof(Locomotion)} not setted {nameof(CharacterController)} for cache");
                _controller = GetComponent<CharacterController>();
            }

            _mainCamera = Camera.main;
        }
        private void OnEnable()
        {
            _inputActions.Enable();
        }

        private void FixedUpdate()
        {
            Vector3 rightSide = _mainCamera.transform.right;
            Vector3 forwardSide = Vector3.Cross(rightSide, Vector3.up);
            Move(rightSide, forwardSide);

            Look(rightSide, forwardSide);

        }

        private void Move(Vector3 rightSide, Vector3 forwardSide)
        {
            Vector2 moveInput = _inputActions.Character.Move.ReadValue<Vector2>();
            Vector3 move = (rightSide * moveInput.x) + (forwardSide * moveInput.y);


            if (move != Vector3.zero)
            {
                OnStartMove?.Invoke();
                move *= _data.moveSpeed * Time.fixedDeltaTime;
                _controller.Move(move);
            }
            else
            {
                OnStopMove?.Invoke();
            }
        }

        private void Look(Vector3 rightSide, Vector3 forwardSide)
        {
            Vector2 lookInput = _inputActions.Character.Look.ReadValue<Vector2>();
            Vector3 look = (rightSide * lookInput.x) + (forwardSide * lookInput.y);
            if (look != Vector3.zero)
            {
                Quaternion direction = Quaternion.LookRotation(look);
                transform.rotation = Quaternion.Lerp(transform.rotation, direction, _data.rotationSpeed * Time.fixedDeltaTime);
            }
        }

        private void OnDisable()
        {
            _inputActions.Disable();
        }
    }
}
