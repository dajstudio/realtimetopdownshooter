using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;

namespace Company.GameShooter
{
    [RequireComponent(typeof(Canvas))]
    public class SingleUICrosshairHolder : MonoBehaviour, ICrosshairHolder
    {
        [SerializeField]
        private CrosshairView _crosshairView;

        [SerializeField]
        private Canvas _canvas;
        public void Initialize()
        {
            if (_canvas == null)
            {
                Debug.LogWarning($"On {name} in component {nameof(SingleUICrosshairHolder)} not setted {nameof(_canvas)} for cache");
                _canvas = GetComponent<Canvas>();
            }
            _canvas.renderMode = RenderMode.ScreenSpaceOverlay;

            if (_crosshairView == null)
            {
                Debug.LogWarning($"On {name} in component {nameof(SingleUICrosshairHolder)} not setted {nameof(_crosshairView)} for cache");
                _crosshairView = GetComponentInChildren<CrosshairView>();
            }
            _crosshairView.Initialize();
        }
        public void Activate(bool isDamagableObject)
        {
            _crosshairView.Activate(isDamagableObject);
        }
        public void Animate()
        {
            _crosshairView.AnimateAsync().Forget();
        }

        public void ChangeColor(Color color)
        {
            _crosshairView.ChangeColor(color);
        }

        public void ChangePosition(Vector3 position)
        {
            _crosshairView.ChangePosition(position);
        }

        public void Deactivate()
        {
            _crosshairView.Deactivate();
        }
    }
}
