using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System;
using System.Threading;
using UnityEngine;

namespace Company.GameShooter
{
    [CreateAssetMenu(fileName = "WeaponType", menuName = "Game/Weapon/Type")]
    public class WeaponType : WearableItemType, IActivation
    {
        public Action<Vector3> OnFire { get; set; }
      

        [ShowInInspector]
        [OdinSerialize]
        private IWeaponBehaviour _weaponBehaviour;

        //TODO: extract abstract class T:WearableItemData for common property like weight and generic class
        [ShowInInspector]
        [OdinSerialize]
        public WeaponData data { get; private set; }

        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
       
        private async UniTaskVoid FireAutoShootAsync(CancellationToken cancellationToken)
        {
            if (!_weaponBehaviour.isCanFire)
            {
                await UniTask.WaitUntil(() => _weaponBehaviour.isCanFire == true, cancellationToken: cancellationToken);
            }
            Fire();

            FireAutoShootAsync(cancellationToken).Forget();
        }
        public void Fire()
        {
            Vector3 hitPoint  = _weaponBehaviour.Fire();
            OnFire?.Invoke(hitPoint);          
        }


        public override void BeforeActivate()
        {
            if (_weaponBehaviour == null)
            {
                Debug.LogError($"{name}: WeaponBehaviour is empty");
                IsCanActivate = false;
            }
            if (data == null)
            {
                Debug.LogError($"{name}: WeaponData is empty");
                IsCanActivate = false;
            }
        }

        public override void FinishActivate()
        {

            _weaponBehaviour.Initialize(data, _currentTransform);


            if (data.isAutoShootingOn)
            {
                _cancellationTokenSource = new CancellationTokenSource();
                FireAutoShootAsync(_cancellationTokenSource.Token).Forget();
            }
          
        }

        public override void Deactivate()
        {
            _cancellationTokenSource.Cancel();
            _cancellationTokenSource.Dispose();
            _weaponBehaviour.Deinitialize();
        }
    }
}
