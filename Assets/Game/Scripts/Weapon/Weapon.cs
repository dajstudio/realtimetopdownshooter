using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Company.GameShooter
{
    public class Weapon : WearableItem<WeaponType>
    {
        public Action<Vector3> OnFire;

        public override void FinishActivate()
        {
            wearableItemType.OnFire += OnFire;
        }
        public override void Deactivate()
        {
            wearableItemType.OnFire -= OnFire;
            base.Deactivate();
        }
    }
}