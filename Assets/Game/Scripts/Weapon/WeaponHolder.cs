using Sirenix.OdinInspector;
using System;
using UnityEngine;

namespace Company.GameShooter
{
    //TODO: extract ISpawn, Extract WeareableItemHolder
    public class WeaponHolder : MonoBehaviour
    {
        public Action<Weapon> OnSpawn;

        public Transform transformForSpawn;
        [Tooltip("Default Weapon for spawn. Prefab")]
        [AssetsOnly]
        public Weapon weaponDefaultForSpawn;
        private Weapon _currentWeapon;

        //Dirty: order by OnEnable, Awake
        private void Awake()
        {
            if(weaponDefaultForSpawn == null)
            {
                Debug.LogWarning($"{name}: {nameof(weaponDefaultForSpawn)} not set.");

                return;
            }

            Spawn(weaponDefaultForSpawn);
        }
        public void Spawn(Weapon weaponForSpawn)
        {
            if(_currentWeapon != null)
            {
                UnspawnCurrentWeapon();
            }

            if (weaponDefaultForSpawn == null)
            {
                Debug.LogWarning($"{name}: {nameof(transformForSpawn)} not set.");

                return;
            }
            

            _currentWeapon = Instantiate(weaponForSpawn, transformForSpawn, false).GetComponent<Weapon>();
            OnSpawn?.Invoke(_currentWeapon);
            _currentWeapon.Activate();
        }
        public void UnspawnCurrentWeapon()
        {
            _currentWeapon.Deactivate();
            Destroy(_currentWeapon.gameObject);
        }
    }
}
