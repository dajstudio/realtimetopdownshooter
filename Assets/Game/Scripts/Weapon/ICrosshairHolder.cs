using Cysharp.Threading.Tasks;
using UnityEngine;

namespace Company.GameShooter
{
    public interface ICrosshairHolder

    {
        public void Activate(bool isDamagableObject);
        public void Deactivate();
        public void Animate();
        public void ChangeColor(Color color);
        public void ChangePosition(Vector3 position);
    }
}
