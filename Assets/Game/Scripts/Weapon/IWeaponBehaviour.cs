using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Company.GameShooter
{
    public interface IWeaponBehaviour
    {
        public bool isCanFire { get; }
        public void Initialize(WeaponData data, Transform currentTransform);
        public Vector3 Fire();
        public void Deinitialize();
    }
}
