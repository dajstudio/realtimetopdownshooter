using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Lean.Pool;
using UnityEngine;

namespace Company.GameShooter
{
    [RequireComponent(typeof(Weapon))]
    public class WeaponVFXHolder : MonoBehaviour
    {
        //TODO: change on weaponVFX class
        private ParticleSystem _weaponVFX;
        private ParticleSystem _hitpointVFX;

        public LeanGameObjectPool poolPrefab;
        private List<LeanGameObjectPool> _pools = new List<LeanGameObjectPool>(2);

        [SerializeField]
        private Weapon _weapon;
       private void OnEnable()
        {
            if (_weapon == null)
            {
                Debug.LogWarning($"On {name} in component {nameof(WeaponVFXHolder)} not setted {nameof(Weapon)} for cache");
                _weapon = GetComponent<Weapon>();
            }

            //Very dirty
            //TODO: add controller pools with pool logic
            foreach (var pool in _pools)
            {
                _pools.Remove(pool);
                Destroy(pool);
            }
            _pools = new List<LeanGameObjectPool>();
            _weaponVFX = _weapon.wearableItemType.data.vfx;
            _hitpointVFX = _weapon.wearableItemType.data.vfxHitPoint;
            AddVFXPool(_weaponVFX);
            AddVFXPool(_hitpointVFX);

            _weapon.OnFire += PlayAllVFX;
        }

        private void AddVFXPool(ParticleSystem vfx)
        {
            LeanGameObjectPool leanGameObjectPool = Instantiate(poolPrefab, transform);
            leanGameObjectPool.Prefab = vfx.gameObject;
            leanGameObjectPool.Preload = 10;
            leanGameObjectPool.Recycle = true;
            leanGameObjectPool.Capacity = 20;
            leanGameObjectPool.PreloadAll();
            _pools.Add(leanGameObjectPool);
        }

        private void PlayAllVFX(Vector3 hitPoint)
        {
            ParticleSystem weaponVFX = LeanPool.Spawn(_weaponVFX,transform);
            PlaySingleVFX(weaponVFX,transform.position).Forget();

            ParticleSystem hitPointVFX = LeanPool.Spawn(_weaponVFX);
            PlaySingleVFX(hitPointVFX,hitPoint).Forget();
        }
        private async UniTaskVoid PlaySingleVFX(ParticleSystem VFX, Vector3 position)
        {
            VFX.transform.position = position;
            VFX.Play();
            await UniTask.WaitWhile(() => VFX.isPlaying);
            LeanPool.Despawn(VFX);
        }
    }
}
