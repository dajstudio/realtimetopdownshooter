using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Company.GameShooter
{
    [CreateAssetMenu(fileName = "WeaponData", menuName = "Game/Weapon/Data")]
    public class WeaponData : SerializedScriptableObject
    {
        //Something syntetic,but imagine real gamedesign comment for task: "want set delay between fire and rate of fire because in wiki info in bullet in seconds for most guns"
        private float _delay = 0.00001f;
        [Tooltip("Delay between fire")]
        [ShowInInspector]
        [OdinSerialize]
        [MinValue(0.000001f)]
        [MaxValue(100000)]
        public float Delay
        {
            set
            {
                
                _delay = value;
                _delay = _delay != 0 ? _delay : 0.000001f;
            }
            get
            {
                return _delay;
            }
        }
        private float _rate;
        [Tooltip("rate of fire = count bullet of seconds")]
        [ShowInInspector]
        [OdinSerialize]
        [MinValue(0.00001f)]
        [MaxValue(100000)]
        public float Rate
        {
            set
            {
                _rate = value;
                _delay = 1 / value;
            }
            get
            {
                return Mathf.Clamp(1f / _delay,0.0001f,100000);
            }
        }

        public int damage;

        public ParticleSystem vfx;
        public ParticleSystem vfxHitPoint;

        private bool _isAutoShootingOn;
        public bool isAutoShootingOn
        {
            get
            {
                return _isAutoShootingOn;           
            }
        }

        public const string PlayerSettingAutoShootDBKey = "PlayerSettingAutoShoot";
        private void OnEnable()
        {
            //Imagine crazy moment that we store our db in playerPrefs by key, not in json and not serialized bytes on disk with no verified on serverside;
            //And we haven't place for default player settings with keys for these
            if (!PlayerPrefs.HasKey(PlayerSettingAutoShootDBKey))
            {
                PlayerPrefs.SetInt(PlayerSettingAutoShootDBKey, 1); //in playerPrefs hasn't bool type
            }

            _isAutoShootingOn = PlayerPrefs.GetInt(PlayerSettingAutoShootDBKey) == 1 ? true : false;
        }
    }

}
