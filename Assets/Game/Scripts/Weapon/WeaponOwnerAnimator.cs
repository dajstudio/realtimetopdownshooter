using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Company.GameShooter
{
    //TODO: extract WearableItemOwnerAnimator
    //For universal shooter kit. need research about that logic animator
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(WeaponHolder))]
    public class WeaponOwnerAnimator : MonoBehaviour
    {
        public const string animationKeyTakeWeapon = "TakeWeapon";
        public const string animationKeyAttack = "Attack";

        [SerializeField]
        private Animator _animator;
        [SerializeField]
        private WeaponHolder _weaponHolder;
        private void OnEnable()
        {
            if (_weaponHolder == null)
            {
                Debug.LogWarning($"{name}/{nameof(WeaponOwnerAnimator)} not setted {nameof(WeaponHolder)} for cache");
                _weaponHolder = GetComponent<WeaponHolder>();
            }

            if (_animator == null)
            {
                Debug.LogWarning($"{name}/{nameof(WeaponOwnerAnimator)} not setted {nameof(Animator)} for cache");
                _animator = GetComponent<Animator>();
            }

            _weaponHolder.OnSpawn += Initialize;
        }
        private void Initialize(Weapon weapon)
        {
            _animator.SetBool(animationKeyTakeWeapon, true);
        }
    }
}