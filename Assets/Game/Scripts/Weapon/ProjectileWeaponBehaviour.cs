using Cysharp.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

namespace Company.GameShooter
{

    [CreateAssetMenu(fileName = "ProjectileWeaponBehaviour", menuName = "Game/Weapon/Behaviour/Projectile")]
    public class ProjectileWeaponBehaviour : ScriptableObject, IWeaponBehaviour
    {
        public float maxDistance = 5;
        [SerializeField]
        private LayerMask _layerMaskForHit;
        [SerializeField]
        private LayerMask _layerMaskForCanFire;

        public bool isCanFire => _timeFromLastFire >= _data?.Delay && _damageReceiver != null;

        private IDamageReceiver _damageReceiver = null;
        private float _timeFromLastFire = 0;
        private WeaponData _data;

        //TODO: move on another layer
        public SingleUICrosshairHolder crosshairDefaultPrefab;
        private SingleUICrosshairHolder _currentCrosshair;
        private Vector3 _defaultPositionOfCrosshair => _currentTransform.position + _currentTransform.rotation * Vector3.left * maxDistance;
        private RaycastHit _hit;
        private Vector3 _hitPoint;
        private bool _isHitSomething;

        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        private Transform _currentTransform;
        public void Initialize(WeaponData data, Transform currentTransform)
        {
            _data = data;
            _damageReceiver = null;
            _timeFromLastFire = 0;
            _currentTransform = currentTransform;

            _cancellationTokenSource = new CancellationTokenSource();

            if (_layerMaskForHit.value == 1 << _layerMaskForCanFire.value)
            {
                Debug.LogError($"{name}: {nameof(_layerMaskForHit)} need contains {nameof(_layerMaskForCanFire)}");
            }

            if (crosshairDefaultPrefab != null)
            {
                SpawnCrosshair(crosshairDefaultPrefab);
            }
            else
            {
                Debug.LogError($"{name}: {nameof(_currentCrosshair)} not set");
            }

            CalculateTimeFromLastFireAsync(_cancellationTokenSource.Token).Forget();
            AimAsync(_cancellationTokenSource.Token).Forget();
        }
       public async UniTaskVoid CalculateTimeFromLastFireAsync(CancellationToken cancellationToken)
        {
            _timeFromLastFire += Time.fixedDeltaTime;
            await UniTask.WaitForFixedUpdate(cancellationToken);

            CalculateTimeFromLastFireAsync(cancellationToken).Forget();
        }
        public void SpawnCrosshair(SingleUICrosshairHolder crosshairPrefab)
        {
            if (_currentCrosshair != null)
            {
                Destroy(_currentCrosshair);
            }
            _currentCrosshair = Instantiate(crosshairPrefab);
            _currentCrosshair.Initialize();
            _currentCrosshair.ChangePosition(_defaultPositionOfCrosshair);
        }

        public Vector3 Fire()
        {

            _timeFromLastFire = 0;
            _currentCrosshair.Animate();
            _damageReceiver.Take(_data.damage);
            return _hitPoint;

        }
        private async UniTask AimAsync(CancellationToken cancellationToken)
        {
            _damageReceiver = Aim();
            await UniTask.WaitForFixedUpdate(cancellationToken);
            AimAsync(cancellationToken).Forget();
        }

        private IDamageReceiver Aim()
        {
            IDamageReceiver damageReceiver = null;
            if (_currentTransform == null)
            {
                return damageReceiver;
            }
            
            Vector3 currentCrosshairPosition = _defaultPositionOfCrosshair;

            _isHitSomething = Physics.Raycast(_currentTransform.position, _currentTransform.rotation * Vector3.left * maxDistance, out _hit, maxDistance, _layerMaskForHit);
            if (_isHitSomething)
            {
                
                currentCrosshairPosition = _hit.point;
                if (_layerMaskForCanFire.value == 1 << _hit.collider.gameObject.layer)
                {
                    _hitPoint = _hit.point;
                    _currentCrosshair.Activate(true);
                    damageReceiver = _hit.collider.GetComponent<IDamageReceiver>();
                }
                else
                {
                    _currentCrosshair.Activate(false);
                }
            }
            else
            {
                _currentCrosshair.Deactivate();
            }

            _currentCrosshair.ChangePosition(currentCrosshairPosition);

            return damageReceiver;
        }

        public void Deinitialize()
        {
            _cancellationTokenSource.Cancel();
            _cancellationTokenSource.Dispose();

        }
    }
}
