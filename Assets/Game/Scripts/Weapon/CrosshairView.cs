using Cysharp.Threading.Tasks;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Company.GameShooter
{
    public class CrosshairView : MonoBehaviour
    {
        public Color colorCanFireObject;
        public Color colorNoCanFire;
        public Color colorDeactivated;

        [SerializeField]
        private Image _image;

        [SerializeField]
        private DOTweenAnimation _dtAnimation;

        private Camera _mainCamera;
        public void Initialize()
        {
            _mainCamera = Camera.main;

            ChangeColor(colorDeactivated);

            if (_dtAnimation == null)
            {
                Debug.LogWarning($"On {name} in component {nameof(CrosshairView)} not setted {nameof(_dtAnimation)} for cache");
                _dtAnimation = GetComponent<DOTweenAnimation>();
            }
        }
        public void Activate(bool isDamagableObject)
        {
            ChangeColor(isDamagableObject ? colorCanFireObject : colorNoCanFire);
        }
        public async UniTaskVoid AnimateAsync()
        {
            _dtAnimation.DOPlay();
            await UniTask.Delay((int)_dtAnimation.duration * 1000);
            _dtAnimation.DORewind();
            _dtAnimation.DORestart();
        }

        public virtual void ChangeColor(Color color)
        {
            _image.color = color;
        }

        public virtual void ChangePosition(Vector3 position)
        {
            //TODO:Move to UICamera
            transform.position = RectTransformUtility.WorldToScreenPoint(_mainCamera, position);
        }

        public void Deactivate()
        {
            ChangeColor(colorDeactivated);
        }
    }
}
