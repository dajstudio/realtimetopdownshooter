Task:
Make gameplay shooter in TPS with autmotic weapon with assets.


**Result: https://youtu.be/S8j00r9R4YQ**

**Artefacts:**
-  Link 1. https://gitlab.com/dajstudio/realtimetopdownshooter/-/raw/main/RealtimeTDShooter_0_001.apk?inline=false
-  Link 2. https://drive.google.com/file/d/1XSyZhIk8p21U2oF5uKX1uSPBOYghux1_/view?usp=sharing


What's done:

- Research of similar assets (20 minutes)
- Familiarization with the internals of Universal Shooter Kit https://assetstore.unity.com/packages/templates/systems/universal-shooter-kit-fps-tps-tds-130255 (20 minutes)
- In-depth unraveling of the code to understand where to add functionality in a beautiful way. Unsuccessful, everything is too interconnected, it would not be possible to make it beautiful (30 minutes)
- Attempt to extract the character animator. Unsuccessful, it overwrites its own animator too much (20 minutes)
- Thinking of a solution (1 hour)
- Setting up a fixed camera with Cinemachine (20 minutes)
- Building the level scene and character, setting targets and sending them along paths (30 minutes)
- Creating weapons and their logic and systems (2 hours)
- Sketching out the architecture for SO (2 hours)
- Adding architecture for wearable items (30 minutes)
- Finding the problem of why the android app building toolkit is downloaded under linux in the windows editor (1 hour)
- Going through the versions of android app building toolkit downloaded before in search of a working one (10 minutes)
- Creating input and calibration (3 hours)
- Creating crosshair logic (2 hours)
- Health system and integration with other systems (1 hour)
- Implementing wearable item logic, refactoring (3 hours)
- Creating VFX (1 hour)
- Assembly and testing (30 minutes)


**What was achieved:**
- Implemented critical functionality
- Outlined an architecture that can be used as a basis for scaling
- Performed minimal optimization


**Assets used:**
- Recorder for video recording
- Cinemachine for camera and moving target control
- UniTask instead of coroutines and async
- LeanPool for shooting VFX
- Dotween for crosshair animation
- Odin for serialization of types that Unity cannot handle


**What to improve:**
- Add IK animation for the character
- Move component presence check to extensions
- Expand the architecture to other program modules, simplify, distribute controversial moments to layers
- Switch to using Pools for creating targets
- Move crosshair to a separate UICamera
- Secure SO Vcontainer
- Connect the database and move clean gameplay data to it from SO
- Create a pool manager
- Move OnScreenCanvas to a separate scene.
